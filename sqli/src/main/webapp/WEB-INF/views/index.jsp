<%@ include file="/WEB-INF/views/includes/references.jsp"%>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="includes/head.jsp" %>
    <body> 
    <%@ include file="includes/nav.jsp" %>
        <div class="wrapper" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 body-content" >
                        <div class="page-header clearfix">
                            <h2 class="pull-left">
                            	<strong><font color="red">Like Our Training?</font></strong>
                            </h2>
                            <a href="create.php" class="btn btn-success pull-right">
                            	Rate Us
                            </a>
                        </div>
                        
						<table id="example" class="table table-striped table-bordered" style="width:100%">';
							<thead>
								<tr>
									<th>ID</th>
									<th>Action</th>
									<th>Name</th>
									<th>Review</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${empty reviewList}">
									<tr><td colspan="5">No Records Found</td></tr>
								</c:if>
								<c:if test="${not empty reviewList}">
									<c:forEach var="review" items="${reviewList}">
										<tr>										
											<td>
												<a href='<%=request.getContextPath()%>/view?id=${review.id}' title='View Record' data-toggle='tooltip'>
													${review.id}
												</a>
											</td>
											<td>
												<a href='<%=request.getContextPath()%>/delete?id=${review.id}' title='Delete Record' data-toggle='tooltip'>
													<span class='glyphicon glyphicon-trash'> Delete</span>
												</a>
											</td>
											<td>${review.name}</td>
											<td>${review.review}</td>
											<td>${review.country}</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
                        
                        
<%--                        
                        <?php
                        // Attempt select query execution
                        $sql = "SELECT * FROM reviews";
                        if (isset($_GET["r_id"]) && !empty($_GET["r_id"])) {
                            $sql .= " where id=" . $_GET['r_id'];
                        }

                        if ($result = mysqli_query($con, $sql)) {
                            if (mysqli_num_rows($result) > 0) {
                                echo '<table id="example" class="table table-striped table-bordered" style="width:100%">';
                                echo "<thead>";
                                echo "<tr>";
                                echo "<th>ID</th>";
                                echo "<th>Action</th>";
                                echo "<th>Name</th>";
                                echo "<th>Review</th>";
                                echo "<th>Country</th>";
                                echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                               
                                while ($row = mysqli_fetch_array($result)) {
                                   
                                    echo "<tr class=''>";
                                    echo "<td><a href='index.php?r_id=" . $row['id'] . "' title='View Record' data-toggle='tooltip'>" . $row['id'] . "</a></td>";
                                    echo "<td>";
                                    echo "<a href='index.php?d_id=" . $row['id'] . "' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'> Delete</span></a>";
                                    echo "</td>";
                                    echo "<td>" . $row['name'] . "</td>";
                                    echo "<td>" . $row['review'] . "</td>";
                                    echo "<td>" . $row['country'] . "</td>";

                                    echo "</tr>";
                                }
                                echo "</tbody>";
                                echo "</table>";
                                // Free result set
                                mysqli_free_result($result);
                            } else {
                                echo "<p style='color:white;font-size:21px'>No records were found.</p>";
                            }
                        } else {
                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
                        }

                        // Close connection
                        mysqli_close($con);
                        ?>
 --%>
                        
                        
                    </div>
                </div>        
            </div>
        </div>
        <%@ include file="includes/footer.jsp" %>
    </body>
</html>